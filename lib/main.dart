import 'package:flutter/material.dart';
import 'package:test_task_flutter/apiservice.dart';
import 'package:test_task_flutter/model/responcemodel.dart';
import 'model/subcategory.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Test Task',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Test Task'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final repository = ApiRepository();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: FutureBuilder<ResponceModel>(
          future: repository.fetchData(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return _getListWidget(snapshot.data, repository);
            } else if (snapshot.hasError) {
              return _getErrorWidget(snapshot.error.toString());
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          },
        ));
  }

  Widget _getListWidget(ResponceModel? data, ApiRepository repository) {
    int n = 2;
    if (data != null) {
      // return ListView.builder(
      //   itemCount: data.hydraMember.length,
      //   itemBuilder: (context, index) {
      //     return _category(data.hydraMember[index], repository);
      //   },
      // );
      final listWidgets = <Widget>[];
      for (var item in data.hydraMember) {
        listWidgets.add(Text(
          item.name,
          style: const TextStyle(
            fontWeight: FontWeight.w900,
            fontSize: 18,
          ),
        ));
        if (item.categories != null && item.categories!.isNotEmpty) {
          for (var itemCategory in item.categories!) {
            listWidgets.add(Padding(
                padding: const EdgeInsets.only(left: 8),
                child: _getCategoryItem(itemCategory, n)));
          }
        }
      }

      return Container(
        padding: const EdgeInsets.all(8),
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: listWidgets,
          ),
        ),
      );
    } else {
      return const Center(child: Text('Error'));
    }
  }

  Widget _getErrorWidget(String message) {
    return Center(child: Text(message));
  }

  Widget _getCategoryItem(Category category, int n) {
    var paddingStart = n * 8.0;
    if (category.categories != null) {
      if (category.categories!.isNotEmpty) {
        final list = <Widget>[];
        list.add(Padding(
          padding: const EdgeInsets.only(top: 4),
          child: Text(
            category.name,
            style: const TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 16,
            ),
          ),
        ));
        for (var item in category.categories!) {
          list.add(_getCategoryItem(item, n));
        }
        n++;
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: list,
        );
      } else {
        return Padding(
          padding: EdgeInsets.only(left: paddingStart),
          child: Text(
            category.name,
            style: const TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16,
            ),
          ),
        );
      }
    } else {
      return Padding(
        padding: EdgeInsets.only(left: paddingStart),
        child: Text(category.name,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 14,
            )),
      );
    }
  }
}
