import 'package:test_task_flutter/model/category.dart';

class ResponceModel {
  late String context;
  late String id;
  late String type;
  late List<HydraMember> hydraMember;
  late int hydraTotalItems;

  ResponceModel(
      {required this.context,
      required this.id,
      required this.type,
      required this.hydraMember,
      required this.hydraTotalItems});

  ResponceModel.fromJson(Map<String, dynamic> json) {
    context = json['@context'];
    id = json['@id'];
    type = json['@type'];
    if (json['hydra:member'] != null) {
      hydraMember = [];
      json['hydra:member'].forEach((v) {
        hydraMember.add(HydraMember.fromJson(v));
      });
    }
    hydraTotalItems = json['hydra:totalItems'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['@context'] = context;
    data['@id'] = id;
    data['@type'] = type;
    data['hydra:member'] = hydraMember.map((v) => v.toJson()).toList();
    data['hydra:totalItems'] = hydraTotalItems;
    return data;
  }
}
