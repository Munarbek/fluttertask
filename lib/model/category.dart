import 'subcategory.dart';

class HydraMember {
  late String type;
  late String typeId;
  late int id;
  late int eeCatId;
  late String name;
  late String slug;
  late List<Category>? categories;

  HydraMember(
      {required this.type,
      required this.typeId,
      required this.id,
      required this.eeCatId,
      required this.name,
      required this.slug,
      required this.categories});

  HydraMember.fromJson(Map<String, dynamic> json) {
    type = json['@type'];
    typeId = json['@id'];
    id = json['id'];
    eeCatId = json['eeCatId'];
    name = json['name'];
    slug = json['slug'];
    categories = [];
    json['categories'].forEach((v) {
      categories?.add(Category.fromJson(v));
    });
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['@type'] = type;
    data['@id'] = id;
    data['id'] = id;
    data['eeCatId'] = eeCatId;
    data['name'] = name;
    data['slug'] = slug;
    if (categories != null) {
      if (categories!.isNotEmpty) {
        data['categories'] = categories?.map((v) => v.toJson()).toList();
      }
    }
    return data;
  }
}
