import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:test_task_flutter/model/responcemodel.dart';

class ApiRepository {
  final Dio _dio = Dio();
  final _baseUrl = 'https://test-docs.stores.kg/api/categories';
  ApiRepository() {
    _dio.options.headers['content-Type'] = 'application/json';
    _dio.options.headers['secretKey'] = 'test_key';
  }
  Future<ResponceModel> fetchData() async {
    try {
      var response = await _dio.get(_baseUrl);
      final jsonData = json.decode(response.data);
      var map = Map<String, dynamic>.from(jsonData);
      return ResponceModel.fromJson(map);
    } on DioError {
      throw const SocketException('message');
    }
  }
}
