class Category {
  late int id;
  late String name;
  late String slug;
  late int eeCatId;
  late String? imageUrl;
  List<Category>? categories;

  Category(
      {required this.id,
      required this.name,
      required this.slug,
      required this.eeCatId,
      required this.imageUrl,
      required this.categories});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    slug = json['slug'];
    eeCatId = json['eeCatId'];
    imageUrl = json['imageUrl'];
    if (json['categories'] != null) {
      categories = [];
      json['categories'].forEach((v) {
        categories?.add(Category.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['slug'] = slug;
    data['eeCatId'] = eeCatId;
    data['imageUrl'] = imageUrl;
    if (categories != null) {
      data['categories'] = categories?.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
